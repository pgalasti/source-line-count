﻿namespace Line_Counter
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbxPattern = new System.Windows.Forms.TextBox();
            this.btnRootDirectory = new System.Windows.Forms.Button();
            this.lblDirectorySelected = new System.Windows.Forms.Label();
            this.btnCount = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbxPattern
            // 
            this.tbxPattern.ForeColor = System.Drawing.Color.Silver;
            this.tbxPattern.Location = new System.Drawing.Point(149, 12);
            this.tbxPattern.Name = "tbxPattern";
            this.tbxPattern.Size = new System.Drawing.Size(272, 20);
            this.tbxPattern.TabIndex = 0;
            this.tbxPattern.Text = "Pattern: *.type1* | *.type2* | ...";
            this.tbxPattern.Enter += new System.EventHandler(this.tbxPattern_Enter);
            this.tbxPattern.Leave += new System.EventHandler(this.tbxPattern_Leave);
            // 
            // btnRootDirectory
            // 
            this.btnRootDirectory.Location = new System.Drawing.Point(12, 12);
            this.btnRootDirectory.Name = "btnRootDirectory";
            this.btnRootDirectory.Size = new System.Drawing.Size(131, 23);
            this.btnRootDirectory.TabIndex = 1;
            this.btnRootDirectory.Text = "Root Source Directory...";
            this.btnRootDirectory.UseVisualStyleBackColor = true;
            this.btnRootDirectory.Click += new System.EventHandler(this.btnRootDirectory_Click);
            // 
            // lblDirectorySelected
            // 
            this.lblDirectorySelected.AutoSize = true;
            this.lblDirectorySelected.Location = new System.Drawing.Point(12, 49);
            this.lblDirectorySelected.Name = "lblDirectorySelected";
            this.lblDirectorySelected.Size = new System.Drawing.Size(111, 13);
            this.lblDirectorySelected.TabIndex = 2;
            this.lblDirectorySelected.Text = "No Directory Selected";
            this.lblDirectorySelected.DoubleClick += new System.EventHandler(this.lblDirectorySelected_Click);
            // 
            // btnCount
            // 
            this.btnCount.Location = new System.Drawing.Point(346, 139);
            this.btnCount.Name = "btnCount";
            this.btnCount.Size = new System.Drawing.Size(75, 23);
            this.btnCount.TabIndex = 3;
            this.btnCount.Text = "Count";
            this.btnCount.UseVisualStyleBackColor = true;
            this.btnCount.Click += new System.EventHandler(this.btnCount_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 174);
            this.Controls.Add(this.btnCount);
            this.Controls.Add(this.lblDirectorySelected);
            this.Controls.Add(this.btnRootDirectory);
            this.Controls.Add(this.tbxPattern);
            this.Name = "Form1";
            this.Text = "Source Line Count";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbxPattern;
        private System.Windows.Forms.Button btnRootDirectory;
        private System.Windows.Forms.Label lblDirectorySelected;
        private System.Windows.Forms.Button btnCount;
    }
}

