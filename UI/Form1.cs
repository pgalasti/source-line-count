﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using Line_Counter.Processing;

namespace Line_Counter
{
    public partial class Form1 : Form
    {

        private String rootSourceFolder;

        public Form1()
        {
            InitializeComponent();
            this.rootSourceFolder = String.Empty;
            this.btnCount.Enabled = false;
        }

        private void tbxPattern_Enter(object sender, EventArgs e)
        {
            this.tbxPattern.Text = String.Empty;
            this.tbxPattern.ForeColor = Color.Black;
        }

        private void tbxPattern_Leave(object sender, EventArgs e)
        {
            if(this.tbxPattern.Text == String.Empty)
            {
                this.tbxPattern.Text = "Pattern: *.type1* | *.type2* | ...";
                this.tbxPattern.ForeColor = Color.Silver;
            }
        }

        private void btnRootDirectory_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog browserDialog = new FolderBrowserDialog();
            browserDialog.RootFolder = System.Environment.SpecialFolder.MyComputer;
            browserDialog.ShowNewFolderButton = false;

            DialogResult result = browserDialog.ShowDialog();
            switch(result)
            {
                case DialogResult.OK:
                    this.rootSourceFolder = this.lblDirectorySelected.Text = browserDialog.SelectedPath;
                    this.btnCount.Enabled = true;
                    break;

                default:
                    return;
            }
        }

        private void lblDirectorySelected_Click(object sender, EventArgs e)
        {
            if(Directory.Exists(this.rootSourceFolder))
            {
                Process.Start("Explorer.exe", this.rootSourceFolder);
            }
        }

        private void btnCount_Click(object sender, EventArgs e)
        {
            LineCounter lineCounter = new LineCounter(this.rootSourceFolder, this.tbxPattern.Text);
            uint lineCount = lineCounter.GetLineCount();
            MessageBox.Show("Line Count: " + lineCount);
        }
    }
}
