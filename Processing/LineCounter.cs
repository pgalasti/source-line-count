﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Line_Counter.Processing
{
    public class LineCounter
    {
        private String rootDirectory;
        private String pattern;

        /// <summary>
        /// The root directory to search all code lines to be counted.
        /// </summary>
        public String RootDirectory
        {
            get { return this.rootDirectory; }
            set 
            { 
                if(!Directory.Exists(value))
                    throw new DirectoryNotFoundException("Directory specified as: " + value + " cannot be found.");

                this.rootDirectory = value; 
            }
        }

        /// <summary>
        /// The search pattern to use for files to be included in the line count.
        /// Pattern: *.type1* | *.type2* | ...
        /// </summary>
        public String SearchPattern
        {
            get { return this.pattern; }
            set
            {
                if (value == String.Empty)
                    throw new InvalidDataException("The search pattern is empty.");

                this.pattern = value;
            }
        }

        /// <summary>
        /// Creates a LineCounter object with a specified root directory and search pattern.
        /// </summary>
        /// <param name="rootDirectory">The root directory path.</param>
        /// <param name="pattern">The search pattern to use for files to be included in the line count.
        /// Pattern: *.type1* | *.type2* | ...
        /// </param>
        public LineCounter(String rootDirectory, String pattern)
        {
            RootDirectory = rootDirectory;
            SearchPattern = pattern;
        }

        public uint GetLineCount()
        {
            List<String> matchingFileList = new List<String>();

            foreach(String pattern in GetSearchPatterns())
                matchingFileList.AddRange( Directory.GetFiles(this.rootDirectory, pattern, SearchOption.AllDirectories) );

            uint lineCount = 0;
            foreach(String filePath in matchingFileList)
                lineCount += GetFileLineCount(filePath);

            return lineCount;
        }


        protected List<String> GetSearchPatterns()
        {
            String[] patterns = this.pattern.Split('|');

            List<String> searchPatterns = new List<String>();
            foreach (String pattern in patterns)
            {
                pattern.Trim();
                searchPatterns.Add(pattern);
            }

            return searchPatterns;
        }

        protected uint GetFileLineCount(String path)
        {
            if (!File.Exists(path))
                throw new FileNotFoundException("Unable to find file with path: " + path + " for line count process.");

            return (uint)File.ReadLines(path).Count();
        }

    }
}
