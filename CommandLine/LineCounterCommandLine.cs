﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Line_Counter.Processing;

namespace Line_Counter.CommandLine
{
    class LineCounterCommandLine
    {
        public LineCounterCommandLine()
        {
            Console.WriteLine("========= Line Counter =========");
        }

        public void ExecuteSearch(String rootPath, String pattern)
        {
            Console.WriteLine("Performing line count on root directory: \r\n" + rootPath);
            Console.WriteLine("Using pattern: \r\n" + pattern );

            LineCounter lineCounter = new LineCounter(rootPath, pattern);
            uint lineCount = lineCounter.GetLineCount();

            Console.WriteLine("Total line count: " + lineCount);
        }
    }
}
