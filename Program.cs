﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using Microsoft.Win32;
using Line_Counter.CommandLine;

namespace Line_Counter
{
    static class Program
    {

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool AllocConsole();

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool FreeConsole();

        [DllImport("kernel32", SetLastError = true)]
        static extern bool AttachConsole(int dwProcessId);

        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", SetLastError = true)]
        static extern uint GetWindowThreadProcessId(IntPtr hWnd, out int lpdwProcessId);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length > 1) // Do UI
            {
                IntPtr ptr = GetForegroundWindow();
                int processId;
                GetWindowThreadProcessId(ptr, out processId);
                Process process = Process.GetProcessById(processId);

                if (process.ProcessName == "cmd")    //Is the uppermost window a cmd process?
                {
                    AttachConsole(process.Id);
                }
                else
                {
                    //no console AND we're in console mode ... create a new console.
                    AllocConsole();
                }

                LineCounterCommandLine commandLine = new LineCounterCommandLine();
                commandLine.ExecuteSearch(args[0], args[1]);
                Console.Write("Press Enter to continue...");
                Console.ReadLine();

                FreeConsole();
            }
            else // Do form
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Form1());
            }
        }
    }
}
